package com.rightcomtech.test.topitopoll.helpers;

/**
 * Created by Thilaw Fabrice on 2018-01-12.
 */

public class Feedbacks {

    public final static String ANGRY = "angry";
    public final static String SAD = "sad";
    public final static String AVERAGE = "average";
    public final static String HAPPY = "happy";
    public final static String VERY_HAPPY = "very happy";
}
