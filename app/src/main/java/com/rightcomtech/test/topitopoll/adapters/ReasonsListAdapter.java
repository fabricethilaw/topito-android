package com.rightcomtech.test.topitopoll.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rightcomtech.test.topitopoll.R;
import com.rightcomtech.test.topitopoll.interfaces.OnReasonListerner;

import java.util.List;

/**
 * Created by Thilaw Fabrice on 2018-01-12.
 */
public class ReasonsListAdapter extends RecyclerView.Adapter<ReasonsListAdapter.ViewHolder> {

    private List<Integer> icons;
    private OnReasonListerner reviewListerner;

    public ReasonsListAdapter(Context context, List<Integer> icons, OnReasonListerner onReasonListerner) {
        this.icons = icons;
        this.reviewListerner = onReasonListerner;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fdbk_review, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        viewHolder.displayIcon(icons.get(viewHolder.getAdapterPosition()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reviewListerner != null) {
                    reviewListerner.onReasonSelected(icons.get(viewHolder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return icons.isEmpty() ? 0 : icons.size();
    }

    public void updateList(List<Integer> collectionList) {
        this.icons = collectionList;
        this.notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;

        ViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
        }

        void displayIcon(final int iconRes) {
            icon.setImageResource(iconRes);

        }
    }


}