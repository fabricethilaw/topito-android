package com.rightcomtech.test.topitopoll.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.rightcomtech.test.topitopoll.R;
import com.rightcomtech.test.topitopoll.helpers.Feedbacks;

import static com.rightcomtech.test.topitopoll.R.id;
import static com.rightcomtech.test.topitopoll.R.layout;

/**
 * Created by Thilaw Fabrice on 2018-01-12.
 */
public class FeedbackActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_feedback);
        toolbar = findViewById(id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.title_activity_feedback);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickFeedback(View view) {
        int feedbackId = view.getId();

        handleFeedback(feedbackId);

    }

    private void handleFeedback(int feedbackId) {
        // TODO Synchroniser le compte des avis recueillies
        switch (feedbackId) {
            case id.fdbkAngry:
                getReasonForBadFdbk(Feedbacks.ANGRY);
                break;

            case id.fdbkSad:
                getReasonForBadFdbk(Feedbacks.SAD);
                break;

            case id.fdbkAverage:
                Snackbar.make(toolbar, R.string.message_feedback_saved, Snackbar.LENGTH_LONG).show();
                break;

            case id.fdbkHappy:
                Snackbar.make(toolbar, R.string.message_feedback_saved, Snackbar.LENGTH_LONG).show();

                break;

            case id.fdbkVeryHappy:
                Snackbar.make(toolbar, R.string.message_feedback_saved, Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    private void getReasonForBadFdbk(String feedback) {
        Intent intent = new Intent(this, ReasonActivity.class);
        intent.putExtra("feedback", feedback);
        startActivity(intent);
    }
}