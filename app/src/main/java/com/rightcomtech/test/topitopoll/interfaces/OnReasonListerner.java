package com.rightcomtech.test.topitopoll.interfaces;

/**
 * Created by Thilaw Fabrice on 2018-01-12.
 */

public interface OnReasonListerner {
    void onReasonSelected(int icon);
}
