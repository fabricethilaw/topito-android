package com.rightcomtech.test.topitopoll.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.rightcomtech.test.topitopoll.R;
import com.rightcomtech.test.topitopoll.adapters.ReasonsListAdapter;
import com.rightcomtech.test.topitopoll.interfaces.OnReasonListerner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thilaw Fabrice on 2018-01-12.
 */
public class ReasonActivity extends AppCompatActivity implements OnReasonListerner {

    private List<Integer> reasonIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        RecyclerView recyclerView = findViewById(R.id.reasonsList);
        setDefaultReasons();
        ReasonsListAdapter reasonsListAdapter = new ReasonsListAdapter(this, reasonIcons, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(reasonsListAdapter);
    }

    private void setDefaultReasons() {
        reasonIcons = new ArrayList<>();
        reasonIcons.add(R.drawable.rzn_support_unreachable);
        reasonIcons.add(R.drawable.rzn_wasting_my_time);
        reasonIcons.add(R.drawable.rzn_support_making_selfies);
        reasonIcons.add(R.drawable.rzn_support_bad_speech);
        reasonIcons.add(R.drawable.rzn_support_not_proactive);
        reasonIcons.add(R.drawable.rzn_support_bribery);
    }

    @Override
    public void onReasonSelected(int icon) {
        Snackbar.make(findViewById(R.id.reasonsList), R.string.message_feedback_saved, Snackbar.LENGTH_LONG).show();
    }
}
